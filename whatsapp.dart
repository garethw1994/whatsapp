void main() {
  WhatsappUser userOne = WhatsappUser(0617895681, 'Gareth');
  WhatsappUser userTwo = WhatsappUser(0735057628, 'Tim');
  
  WhatsappGroup group = WhatsappGroup('Family', userOne.GetUserProfile());
  
  group.AddUser(userTwo.GetUserProfile());
  
  List admins = group.ViewAdmins();
  
  List users = group.ViewUsers();
  
  print(users);
}


class WhatsappUser {
  int cellNumber;
  String displayName;
  
  WhatsappUser(int cellNumber, String name) {
    this.cellNumber = cellNumber;
    this.displayName = name;
  }
  
  GetUserProfile() {
    return { this.cellNumber, this.displayName };
  }
}

class WhatsappGroup {
   String groupName;
   List<Object> admins = []; 
   List<Object> users = [];
  
   WhatsappGroup(String name, Object user) {
    this.groupName = name; 
    this.admins.add(user); 
   } 
    
   void AddUser(Object user) {
     users.add(user);
   } 
  
   ViewUsers() {
     return users;
   } 
  
  ViewAdmins() {
    return admins;
  }
}